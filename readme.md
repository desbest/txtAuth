# txtAuth 1.0

Created by [txtbox](https://archive.is/lRKVm) who now runs [cbox](http://cbox.ws)
Maintained by [desbest](http://desbest.com)

Protect pages without having to mess around with .htaccess or complicated 
scripts. txtAuth allows you to protect individual pages with their own 
login details and custom descriptive text. It also supports "remember-me" 
cookies.


## [ INSTALLATION ]

 * Extract all the files to their own folder.
 * Open txtauth.php in any text editor (e.g. Notepad), and change the 
   default username and password settings.
 * Save the file and upload it to your website. 
 * To test it, upload secret.php as well, and visit it online. Log in with 
   the username and password you just specified.


## [ PROTECTING PAGES ]

   Add the following to the top of any .php page to be protected:

   <?
   require('txtauth.php');
   ?>

   In order to protect a page, it must be a PHP file. Any HTML page, 
   e.g. mypage.html, can be renamed mypage.php, and it will work, providing 
   the server supports PHP (and if it doesn't, txtAuth won't work!)

   You can override any of the config options set in txtauth.php 
   (the $tacfg[...] variables). This is especially useful if you want to 
   protect more than one page, using different login details. Simply add 
   the variables you want to override before the require(), like so:

   <?
   $tacfg['uname'] = 'demo';
   $tacfg['pword'] = 'demo';
   require('txtauth.php');
   ?>

   Note that it is $tacfg[...] and NOT $tacfgd[...]. The "d" is missing when 
   overriding.


## [ MINI FAQ ]

 Q: I get an error saying "Cannot send session cache limiter ...."

 A: In your protected page(s), the opening tag "<?" before the require() 
    must be the _first_ thing on the page. No other text (even spaces or empty 
    lines) must appear or be included before the "<?".


 Q: I can log in but I don't stay logged-in when I click through to other 
    protected pages.
 
 A: If you have scripted your own session support into your protected page, 
    check that it isn't destroying the txtAuth session variable. 
    If you don't use your own sessions (or don't know what they are even), 
    check that the $tacfgd['ownsessions'] variable is set to false in txtauth.php.


 Q: I have a different problem!

 A: Use the support options at my website listed above.


## [ CHANGELOG ]

 * v1.0 [04-05-09]
   - First release.
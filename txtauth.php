<?
    /****************************
    * txtAuth v1.0              * 
    * �2004 - desbest           *
    * http://github.com/desbest *
    ****************************/


// Default config options. 
// Override these by pasting them in the form $tacfg['....'] above the require() code in the actual 
// page to be protected.

// Username and password
$tacfgd['uname'] = 'uname';
$tacfgd['pword'] = 'pword';

// Title of page.
$tacfgd['title'] = 'txtAuth Login';

// Text to appear just above login form.
$tacfgd['helptext'] = 'To proceed, please log in using the form below';


// Set to true to enable the optional remember-me feature, which stores encrypted login details to 
// allow users to be logged-in automatically on their return. Turn off for a little extra security.
$tacfgd['allowrm'] = true;

// If you have multiple protected pages, and there's more than one username / password combination, 
// you need to group each combination under a distinct rmgroup so that the remember-me feature 
// knows which login details to use.
$tacfgd['rmgroup'] = 'default';

// Set to true if you use your own sessions within your protected page, to stop txtAuth interfering. 
// In this case, you _must_ call session_start() before you require() txtAuth. Logging out will not 
// destroy the session, so that is left up to you.
$tacfgd['ownsessions'] = false;




foreach ($tacfgd as $key => $val) {
  if (!isset($tacfg[$key])) $tacfg[$key] = $val;
}

if (!$tacfg['ownsessions']) {
  session_name('txtauth');
  session_start();
}

// Logout attempt made. Deletes any remember-me cookie as well
if (isset($_GET['txtlogout']) || isset($_POST['txtlogout'])) {
  setcookie('txtauth_'.$rmgroup, '', time()-86400*14);
  if (!$tacfg['ownsessions']) {
    $_SESSION = array();
    session_destroy();
  }
  else $_SESSION['txtauthin'] = false;
}
// Login attempt made
elseif (isset($_POST['login'])) {
  if ($_POST['uname'] == $tacfg['uname'] && $_POST['pword'] == $tacfg['pword']) {
    $_SESSION['txtauthin'] = true;
    if ($_POST['rm']) {
      // Set remember-me cookie for 2 weeks
      setcookie('txtauth_'.$rmgroup, md5($tacfg['uname'].$tacfg['pword']), time()+86400*14);
    }
  }
  else $err = 'Invalid username or password';
}
// Remember-me cookie exists
elseif (isset($_COOKIE['txtauth_'.$rmgroup])) {
  if (md5($tacfg['uname'].$tacfg['pword']) == $_COOKIE['txtauth_'.$rmgroup] && $tacfg['allowrm']) {
    $_SESSION['txtauthin'] = true;
  }
  else $err = 'Remember-me cookie disabled or invalid';
}
if (!$_SESSION['txtauthin']) {
?>
<html>
<head>
<title><?=$tacfg['title']?></title>
<style type="text/css">
body {
  font-family: Arial, sans-serif;
  font-size: 10pt;
}
td {
  font-family: Arial, sans-serif;
  font-size: 10pt;
}
.grey {
  font-family: Arial, sans-serif;
  font-size: 10pt;
  color: #878787;
}
.txtbox {
  font-family: Arial, sans-serif;
  font-size: 10pt;
  background-color: #ffffff;
  border: #dedede 1px solid;
}
.smalltxt {
  font-size: 8pt;
  color: #cdcdcd;
}
.smalltxt A {
  color: #cdcdcd;
}
</style>
</head>

<body>
<div style="font-size: 14pt;" align="center"><?=$tacfg['title']?></div>
<hr width="300" size="1" noshade color="#cdcdcd">
<p>
<div align="center" class="grey">
<?=$tacfg['helptext']?>
</div>
<p>
<?
if (isset($_SERVER['REQUEST_URI'])) $action = $_SERVER['REQUEST_URI'];
else $action = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
if (strpos($action, 'txtlogout=1', strpos($action, '?')) !== false) $action = str_replace('txtlogout=1', '', $action);
?>
<form name="txtauth" action="<?=$action?>" method="post">
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#efefef" align="center" style="border: #dedede 3px double;">
<?=(isset($err))?'<tr><td colspan="2" align="center"><font color="red">'.$err.'</font></td></tr>':''?>
<?if (isset($tacfg['uname'])) {?>
<tr><td>username:</td><td><input type="text" name="uname" value="" size="20" maxlength="100" class="txtbox"></td></tr>
<?}?>
<tr><td>password:</td><td><input type="password" name="pword" value="" size="20" maxlength="100" class="txtbox"></td></tr>
<?if ($tacfg['allowrm']) {?>
<tr><td align="left"><input type="submit" name="login" value=" login ">
</td><td align="right"><input type="checkbox" name="rm" id="rm"><label for="rm">Log me in automatically</label></td></tr>
<?} else {?>
<tr><td colspan="2" align="center"><input type="submit" name="login" value=" login "></td></tr>
<?}?>
</table>
</form>

<br><br><br>
<small>powered by <a href="http://github.com/desbest">&copy; desbest</a> & <a href=<a href="http://github.com/desbest">Powered bt Txtauth</a></small></div>

</body>
</html>
<?
  // Don't delete this!
  exit();
}
?>